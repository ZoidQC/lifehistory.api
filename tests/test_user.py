import unittest

from flask import json

from tests.test_base import TestBase


class TestUser(TestBase):
    def test_create_user(self):
        response = self.app.post('/api/users',
                                 data=json.dumps(dict(name='name', username='dummy', password='12345')),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 201)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['username'], 'dummy')

    def test_authenticate_fail(self):
        response = self.app.post('/api/authenticate',
                                 data=json.dumps(dict(username='account1', password='wrong_password')),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['authenticate_result'], 0)

    def test_authenticate_success(self):
        response = self.app.post('/api/authenticate',
                                 data=json.dumps(dict(username='account1', password='password1')),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['authenticate_result'], 1)


if __name__ == '__main__':
    unittest.main()
