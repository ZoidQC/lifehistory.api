import base64
import unittest

from passlib.apps import custom_app_context as pwd_context

from lifehistory_api.app import app, db
from tests.factories import UserFactory, ActivityTypeFactory, ActivityFactory, DayFactory, LifeEntryFactory, \
    LifeEntryActivityFactory


def get_authorization_header_for_user(user_id):
    return {
        'Authorization': 'Basic ' + base64.b64encode(f'account{user_id}:password{user_id}'.encode()).decode("utf-8")
    }


def mock_data():
    db.session.add(UserFactory(name='user1', username='account1', password_hash=pwd_context.encrypt('password1')))
    db.session.add(UserFactory(name='user2', username='account2', password_hash=pwd_context.encrypt('password2')))

    activity_type1 = ActivityTypeFactory(user_id=1, name='activity_type1')
    activity_type2 = ActivityTypeFactory(user_id=2, name='activity_type2')

    db.session.add(activity_type1)
    db.session.add(activity_type2)
    db.session.add(ActivityTypeFactory(user_id=1, name='activity_type_without_activity'))

    activity1 = ActivityFactory(user_id=1, name='activity1', activity_type=activity_type1)
    activity2 = ActivityFactory(user_id=2, name='activity2', activity_type=activity_type2)

    db.session.add(activity1)
    db.session.add(activity2)
    db.session.add(
        ActivityFactory(user_id=1, name='activity_without_life_entry_activity', activity_type=activity_type1))

    db.session.add(DayFactory(user_id=1))
    db.session.add(DayFactory(user_id=2))

    db.session.add(LifeEntryFactory(user_id=1, day_id=1))
    db.session.add(LifeEntryFactory(user_id=2, day_id=2))

    db.session.add(LifeEntryActivityFactory(user_id=1, life_entry_id=1, activity=activity1))
    db.session.add(LifeEntryActivityFactory(user_id=2, life_entry_id=2, activity=activity2))

    db.session.commit()


class TestBase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

        db.create_all()
        mock_data()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
