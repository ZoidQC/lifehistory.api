import unittest

from flask import json

from lifehistory_api.models import LifeEntryActivity
from tests.factories import LifeEntryActivityFactory
from tests.test_base import TestBase, get_authorization_header_for_user


class TestLifeEntryActivity(TestBase):
    def test_create_life_entry_activity(self):
        description = 'test_description'
        life_entry_activity = LifeEntryActivityFactory(user_id=1, activity_id=1, description=description)

        response = self.app.post('/api/life_entry_activities',
                                 headers=get_authorization_header_for_user(1),
                                 data=json.dumps(LifeEntryActivity.serialize(life_entry_activity)),
                                 content_type='application/json')

        self.assertEqual(response.status_code, 201)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['description'], description)

    def test_get_life_entry_activity(self):
        response = self.app.get('/api/life_entry_activities/1',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['rating'], 10)

    def test_get_life_entry_activity_not_owner(self):
        response = self.app.get('/api/life_entry_activities/1',
                                headers=get_authorization_header_for_user(2))

        self.assertEqual(response.status_code, 401)

    def test_update_life_entry_activities(self):
        description = 'test_update_description'
        life_entry_activity = LifeEntryActivityFactory(user_id=1, activity_id=1, description=description)

        response = self.app.put('/api/life_entry_activities/1',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(LifeEntryActivity.serialize(life_entry_activity)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['description'], description)

    def test_update_life_entry_activities_not_owner(self):
        life_entry_activity = LifeEntryActivityFactory()

        response = self.app.put('/api/life_entry_activities/2',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(LifeEntryActivity.serialize(life_entry_activity)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 401)

    def test_delete_life_entry_activities(self):
        response = self.app.delete('/api/life_entry_activities/1',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

    def test_delete_life_entry_activities_not_owner(self):
        response = self.app.delete('/api/life_entry_activities/2',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 401)


if __name__ == '__main__':
    unittest.main()
