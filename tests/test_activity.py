import unittest

from flask import json

from lifehistory_api.models import Activity
from tests.factories import ActivityFactory
from tests.test_base import TestBase, get_authorization_header_for_user


class TestActivity(TestBase):
    def test_create_activity(self):
        name = 'test_activity'
        activity = ActivityFactory(name=name, activity_type_id=1)

        response = self.app.post('/api/activities',
                                 headers=get_authorization_header_for_user(1),
                                 data=json.dumps(Activity.serialize(activity)),
                                 content_type='application/json')

        self.assertEqual(response.status_code, 201)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['name'], name)

    def test_get_activities(self):
        response = self.app.get('/api/activities',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data[0]['id'], 1)
        self.assertEqual(data[0]['name'], 'activity1')

    def test_get_activity(self):
        response = self.app.get('/api/activities/1',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['name'], 'activity1')

    def test_get_activity_not_owner(self):
        response = self.app.get('/api/activities/1',
                                headers=get_authorization_header_for_user(2))

        self.assertEqual(response.status_code, 401)

    def test_update_activities(self):
        name = 'test_update_activity'
        activity = ActivityFactory(name=name)

        response = self.app.put('/api/activities/1',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(Activity.serialize(activity)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['name'], name)

    def test_update_activities_with_bad_activity_type(self):
        name = 'test_update_activity'
        activity = ActivityFactory(name=name, activity_type_id=2)

        response = self.app.put('/api/activities/1',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(Activity.serialize(activity)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 401)

    def test_update_activities_not_owner(self):
        activity = ActivityFactory(name='test_update_activities_not_owner')

        response = self.app.put('/api/activities/2',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(Activity.serialize(activity)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 401)

    def test_delete_activities(self):
        response = self.app.delete('/api/activities/3',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

    def test_delete_activities_not_owner(self):
        response = self.app.delete('/api/activities/2',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 401)

    def test_search_activities(self):
        response = self.app.get('/api/activities/search/activity',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['name'], 'activity1')

    def test_search_activities2(self):
        response = self.app.get('/api/activities/search/test',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(len(data), 0)


if __name__ == '__main__':
    unittest.main()
