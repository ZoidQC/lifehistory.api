from datetime import datetime

import factory

from lifehistory_api.models import LifeEntryActivity, LifeEntry, Day, Activity, ActivityType, User


class UserFactory(factory.Factory):
    class Meta:
        model = User

    created_date = datetime.utcnow()


class ActivityTypeFactory(factory.Factory):
    class Meta:
        model = ActivityType

    user_id = 1
    created_date = datetime.utcnow()
    name = factory.Sequence(lambda n: 'activity_type{0}'.format(n))
    show_rating = True
    show_quantity = True


class ActivityFactory(factory.Factory):
    class Meta:
        model = Activity

    user_id = 1
    created_date = datetime.utcnow()
    name = factory.Sequence(lambda n: 'activity{0}'.format(n))
    activity_type_id = 1
    activity_type = factory.SubFactory(ActivityTypeFactory)


class DayFactory(factory.Factory):
    class Meta:
        model = Day

    user_id = 1
    created_date = datetime.utcnow()
    date = datetime.utcnow().date()
    note = factory.Faker('text')


class LifeEntryFactory(factory.Factory):
    class Meta:
        model = LifeEntry

    user_id = 1
    created_date = datetime.utcnow()
    day_id = 1
    start_time = datetime.strptime('14:00:00', '%H:%M:%S').time()
    end_time = datetime.strptime('18:00:00', '%H:%M:%S').time()


class LifeEntryActivityFactory(factory.Factory):
    class Meta:
        model = LifeEntryActivity

    user_id = 1
    created_date = datetime.utcnow()
    life_entry_id = 1
    description = factory.Faker('name')
    quantity = 1
    rating = 10
    activity_id = 1
    activity = factory.SubFactory(ActivityFactory)
