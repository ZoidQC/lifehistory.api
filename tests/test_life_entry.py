import unittest
from datetime import datetime

from flask import json

from lifehistory_api.models import LifeEntry
from tests.factories import LifeEntryFactory
from tests.test_base import TestBase, get_authorization_header_for_user


class TestLifeEntry(TestBase):
    def test_create_life_entry(self):
        start_time = '15:30:00'
        life_entry = LifeEntryFactory(user_id=1, start_time=datetime.strptime(start_time, '%H:%M:%S').time())

        response = self.app.post('/api/life_entries',
                                 headers=get_authorization_header_for_user(1),
                                 data=json.dumps(LifeEntry.serialize(life_entry)),
                                 content_type='application/json')

        self.assertEqual(response.status_code, 201)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['start_time'], start_time)

    def test_get_life_entry(self):
        response = self.app.get('/api/life_entries/1',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['start_time'], '14:00:00')

    def test_get_life_entry_not_owner(self):
        response = self.app.get('/api/life_entries/1',
                                headers=get_authorization_header_for_user(2))

        self.assertEqual(response.status_code, 401)

    def test_update_life_entries(self):
        start_time = '15:45:00'
        life_entry = LifeEntryFactory(user_id=1, start_time=datetime.strptime(start_time, '%H:%M:%S').time())

        response = self.app.put('/api/life_entries/1',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(LifeEntry.serialize(life_entry)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['start_time'], start_time)

    def test_update_life_entries_not_owner(self):
        life_entry = LifeEntryFactory()

        response = self.app.put('/api/life_entries/2',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(LifeEntry.serialize(life_entry)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 401)

    def test_delete_life_entries(self):
        response = self.app.delete('/api/life_entries/1',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

    def test_delete_life_entries_not_owner(self):
        response = self.app.delete('/api/life_entries/2',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 401)


if __name__ == '__main__':
    unittest.main()
