import json
from datetime import datetime

from flask import request, jsonify, abort, url_for, Response, g
from sqlalchemy import or_, func

from lifehistory_api.app import app, db, auth
from lifehistory_api.models import User, ActivityType, Activity, Day, LifeEntry, LifeEntryActivity
from lifehistory_api.utils import get_date_string, get_time_string


@auth.verify_password
def verify_password(username_or_token, password):
    return User.verify_user_and_password(username_or_token, password)


@app.route('/api/')
def root():
    return 'The API works :)'


@app.route('/api/authenticate', methods=['POST'])
def authenticate():
    username = request.json.get('username')
    password = request.json.get('password')

    if User.verify_user_and_password(username, password):
        result = 1
    else:
        result = 0

    return jsonify({'authenticate_result': result}), 200


@app.route('/api/users', methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    name = request.json.get('name')

    if username is None or password is None or name is None:
        abort(400)
    if User.query.filter_by(username=username).first() is not None:
        abort(400)

    user = User(username=username, name=name)
    user.created_date = datetime.utcnow()
    user.hash_password(password)

    db.session.add(user)
    db.session.commit()

    return (jsonify({'username': user.username, 'userId': user.id}), 201,
            {'Location': url_for('get_user', id=user.id, _external=True)})


@app.route('/api/users')
@auth.login_required
def get_user():
    user = User.query.get(g.user.id)
    if not user:
        abort(400)
    return jsonify({'username': user.username, 'userId': user.id})


@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@app.route('/api/activity_types')
@auth.login_required
def get_activity_types():
    activity_types = ActivityType.query.filter_by(user_id=g.user.id).all()
    serialized_array = [ActivityType.serialize(activity_type) for activity_type in activity_types]
    return Response(json.dumps(serialized_array), mimetype='application/json')


@app.route('/api/activity_types', methods=['POST'])
@auth.login_required
def new_activity_type():
    user_id = g.user.id
    name = request.json.get('name')
    show_rating = request.json.get('show_rating')
    show_quantity = request.json.get('show_quantity')

    activity_type = ActivityType()
    activity_type.created_date = datetime.utcnow()
    activity_type.user_id = user_id
    activity_type.name = name
    activity_type.show_rating = show_rating
    activity_type.show_quantity = show_quantity

    db.session.add(activity_type)
    db.session.commit()
    return (jsonify(ActivityType.serialize(activity_type)), 201,
            {'Location': url_for('get_activity_type', id=activity_type.id, _external=True)})


@app.route('/api/activity_types/<int:id>')
@auth.login_required
def get_activity_type(id):
    activity_type = ActivityType.query.get(id)
    if not activity_type:
        abort(400)
    if activity_type.user_id != g.user.id:
        abort(401)
    return jsonify(ActivityType.serialize(activity_type))


@app.route('/api/activity_types/<int:id>', methods=['PUT'])
@auth.login_required
def update_activity_type(id):
    activity_type = ActivityType.query.get(id)
    if not activity_type:
        abort(400)
    if activity_type.user_id != g.user.id:
        abort(401)

    name = request.json.get('name')
    show_rating = request.json.get('show_rating')
    show_quantity = request.json.get('show_quantity')

    activity_type.name = name
    activity_type.show_rating = show_rating
    activity_type.show_quantity = show_quantity

    db.session.commit()

    return jsonify(ActivityType.serialize(activity_type))


@app.route('/api/activity_types/<int:id>', methods=['DELETE'])
@auth.login_required
def delete_activity_type(id):
    activity_type = ActivityType.query.get(id)
    if not activity_type:
        abort(400)
    if activity_type.user_id != g.user.id:
        abort(401)

    activities = Activity.query.filter_by(user_id=g.user.id, activity_type_id=activity_type.id).all()
    if len(activities) > 0:
        abort(409)

    db.session.delete(activity_type)
    db.session.commit()

    return ''


@app.route('/api/activity_types/search/<search_term>')
@auth.login_required
def search_activity_type(search_term):
    activity_types = ActivityType.query.filter_by(user_id=g.user.id).filter(
        ActivityType.name.like('%' + search_term + '%')).all()
    serialized_array = [ActivityType.serialize(activity_type) for activity_type in activity_types]
    return Response(json.dumps(serialized_array), mimetype='application/json')


@app.route('/api/activities')
@auth.login_required
def get_activities():
    activities = Activity.query.filter_by(user_id=g.user.id).all()
    serialized_array = [Activity.serialize(activity) for activity in activities]
    return Response(json.dumps(serialized_array), mimetype='application/json')


@app.route('/api/activities', methods=['POST'])
@auth.login_required
def new_activity():
    user_id = g.user.id
    name = request.json.get('name')
    activity_type_id = request.json.get('activity_type_id')

    if not activity_type_id:
        abort(400)

    activity_type = ActivityType.query.get(activity_type_id)
    if not activity_type:
        abort(400)
    if activity_type.user_id != g.user.id:
        abort(401)

    activity = Activity()
    activity.created_date = datetime.utcnow()
    activity.user_id = user_id
    activity.name = name
    activity.activity_type_id = activity_type_id

    db.session.add(activity)
    db.session.commit()
    return (jsonify(Activity.serialize(activity)), 201,
            {'Location': url_for('get_activity', id=activity.id, _external=True)})


@app.route('/api/activities/<int:id>')
@auth.login_required
def get_activity(id):
    activity = Activity.query.get(id)
    if not activity:
        abort(400)
    if activity.user_id != g.user.id:
        abort(401)
    return jsonify(Activity.serialize(activity))


@app.route('/api/activities/<int:id>', methods=['PUT'])
@auth.login_required
def update_activity(id):
    activity = Activity.query.get(id)
    if not activity:
        abort(400)
    if activity.user_id != g.user.id:
        abort(401)

    name = request.json.get('name')
    activity_type_id = request.json.get('activity_type_id')

    activity_type = ActivityType.query.get(activity_type_id)
    if not activity_type:
        abort(400)
    if activity_type.user_id != g.user.id:
        abort(401)

    activity.name = name
    activity.activity_type_id = activity_type_id

    db.session.commit()

    return jsonify(Activity.serialize(activity))


@app.route('/api/activities/<int:id>', methods=['DELETE'])
@auth.login_required
def delete_activity(id):
    activity = Activity.query.get(id)
    if not activity:
        abort(400)
    if activity.user_id != g.user.id:
        abort(401)

    life_entry_activities = LifeEntryActivity.query.filter_by(user_id=g.user.id, activity_id=activity.id).all()
    if len(life_entry_activities) > 0:
        abort(409)

    db.session.delete(activity)
    db.session.commit()

    return ''


@app.route('/api/activities/<int:id>/replace', methods=['POST'])
@auth.login_required
def replace_activity(id):
    original_activity = Activity.query.get(id)
    if not original_activity:
        abort(400)
    if original_activity.user_id != g.user.id:
        abort(401)

    activity_ids = request.json.get('activity_ids')
    description = request.json.get('description')

    activities = []
    for activity_id in activity_ids:
        activity = Activity.query.get(activity_id)
        if not activity:
            abort(400)
        if activity.user_id != g.user.id:
            abort(401)
        activities.append(activity)

    life_entry_activities = LifeEntryActivity.query.filter_by(user_id=g.user.id, activity_id=original_activity.id).all()

    for life_entry_activity in life_entry_activities:
        for index, activity in enumerate(activities):
            if index == 0:
                life_entry_activity.activity_id = activity.id
                if description:
                    life_entry_activity.description = "{0} {1}".format(description, life_entry_activity.description)
            else:
                add_life_entry_activity = LifeEntryActivity()
                add_life_entry_activity.created_date = datetime.utcnow()
                add_life_entry_activity.user_id = g.user.id
                add_life_entry_activity.life_entry_id = life_entry_activity.life_entry_id
                add_life_entry_activity.activity_id = activity.id
                add_life_entry_activity.quantity = life_entry_activity.quantity
                add_life_entry_activity.rating = life_entry_activity.rating

                db.session.add(add_life_entry_activity)
            db.session.commit()

    db.session.delete(original_activity)
    db.session.commit()
    return ''


@app.route('/api/activities/search/<search_term>')
@auth.login_required
def search_activity(search_term):
    activities = Activity.query.filter_by(user_id=g.user.id).filter(Activity.name.like('%' + search_term + '%')).all()
    serialized_array = [Activity.serialize(activity) for activity in activities]
    return Response(json.dumps(serialized_array), mimetype='application/json')


@app.route('/api/activities/<int:id>/descriptions')
@auth.login_required
def get_activity_descriptions(id):
    activity = Activity.query.get(id)
    if not activity:
        abort(400)
    if activity.user_id != g.user.id:
        abort(401)

    args = request.args
    filter_arg = args.get("filter")

    query = db.session.query(LifeEntryActivity.description, func.max(LifeEntryActivity.created_date)) \
        .filter(LifeEntryActivity.user_id == g.user.id,
                LifeEntryActivity.activity_id == activity.id,
                LifeEntryActivity.description != '') \
        .group_by(LifeEntryActivity.description) \
        .order_by(LifeEntryActivity.created_date.desc())

    if filter_arg is not None:
        query = query.filter(LifeEntryActivity.description.like('%' + filter_arg + '%'))

    query_result = query.limit(20).all()
    result = [result_row[0] for result_row in query_result]

    return Response(json.dumps(result), mimetype='application/json')


@app.route('/api/activities/best_of', methods=['POST'])
@auth.login_required
def activities_best_of():
    activity_type_id = request.json.get('activity_type_id')
    activity_id = request.json.get('activity_id')
    if not activity_type_id and not activity_id:
        abort(400)

    start_date = request.json.get('start_date')
    end_date = request.json.get('end_date')
    time_group = request.json.get('time_group')  # Possible values are 'all_time', 'year' and 'month'
    text_group = request.json.get('text_group')  # Possible values are 'name', 'description' and 'name_description'
    best_of = request.json.get('best_of')  # Possible values are 'rating' and 'count'

    time_group_format = ''
    if time_group == 'year':
        time_group_format = '%Y'
    elif time_group == 'month':
        time_group_format = '%Y-%m'

    best_of_field = func.count(LifeEntryActivity.id)
    if best_of == 'rating':
        best_of_field = func.avg(LifeEntryActivity.rating)

    text_group_field = Activity.name
    if text_group == 'description':
        text_group_field = func.coalesce(LifeEntryActivity.description, '')
    elif text_group == 'name_description':
        text_group_field = Activity.name + ' - ' + func.coalesce(LifeEntryActivity.description, '')

    time_group_field = func.strftime(time_group_format, Day.date)
    query = db.session.query(best_of_field.label('best_of_value')) \
        .add_column(text_group_field.label('name')) \
        .add_column(time_group_field.label('time_group')) \
        .join(LifeEntry) \
        .join(Day) \
        .join(LifeEntryActivity.activity) \
        .join(Activity.activity_type) \
        .filter(LifeEntryActivity.user_id == g.user.id) \
        .group_by(text_group_field) \
        .group_by(time_group_field) \
        .order_by(time_group_field.desc(), best_of_field.desc())

    if activity_type_id is not None:
        query = query.filter(ActivityType.id == activity_type_id)

    if activity_id is not None:
        query = query.filter(Activity.id == activity_id)

    if start_date is not None:
        query = query.filter(Day.date >= start_date)

    if end_date is not None:
        query = query.filter(Day.date <= end_date)

    if best_of == 'rating':
        query = query.filter(ActivityType.show_rating).filter(LifeEntryActivity.rating.isnot(None))

    def serialize(result_row):
        return {
            'best_of_value': result_row.best_of_value,
            'name': result_row.name,
            'time_group': result_row.time_group,
        }

    query_result = query.all()
    serialized_array = [serialize(result_row) for result_row in query_result]
    return Response(json.dumps(serialized_array), mimetype='application/json')


@app.route('/api/days', methods=['POST'])
@auth.login_required
def new_day():
    user_id = g.user.id
    date = datetime.strptime(request.json.get('date'), '%Y-%m-%d')
    note = request.json.get('note')

    if Day.query.filter((Day.user_id == g.user.id) & (Day.date == date)).first() is not None:
        abort(400)

    day = Day()
    day.created_date = datetime.utcnow()
    day.user_id = user_id
    day.date = date
    day.note = note

    db.session.add(day)
    db.session.commit()
    return (jsonify(Day.serialize(day)), 201,
            {'Location': url_for('get_day', id=day.id, _external=True)})


@app.route('/api/days/<int:id>')
@auth.login_required
def get_day(id):
    day = Day.query.get(id)
    if not day:
        abort(400)
    if day.user_id != g.user.id:
        abort(401)
    return jsonify(Day.serialize(day))


@app.route('/api/days/<selected_date>')
@auth.login_required
def get_day_by_date(selected_date):
    date = datetime.strptime(selected_date, '%Y-%m-%d')
    day = Day.query.filter((Day.user_id == g.user.id) & (Day.date == date)).first()
    if not day:
        abort(404)
    return jsonify(Day.serialize(day))


@app.route('/api/days/<int:id>', methods=['PUT'])
@auth.login_required
def update_day(id):
    day = Day.query.get(id)
    if not day:
        abort(400)
    if day.user_id != g.user.id:
        abort(401)

    note = request.json.get('note')

    day.note = note
    db.session.commit()

    return jsonify(Day.serialize(day))


@app.route('/api/life_entries', methods=['POST'])
@auth.login_required
def new_life_entry():
    user_id = g.user.id
    day_id = request.json.get('day_id')
    request_start_time = request.json.get('start_time')
    request_end_time = request.json.get('end_time')

    start_time = datetime.strptime(request_start_time, '%H:%M:%S').time()
    if request_end_time:
        end_time = datetime.strptime(request_end_time, '%H:%M:%S').time()
    else:
        end_time = None

    day = Day.query.get(day_id)
    if not day:
        abort(400)
    if day.user_id != g.user.id:
        abort(401)

    life_entry = LifeEntry()
    life_entry.created_date = datetime.utcnow()
    life_entry.user_id = user_id
    life_entry.day_id = day_id
    life_entry.start_time = start_time
    life_entry.end_time = end_time

    db.session.add(life_entry)
    db.session.commit()
    return (jsonify(LifeEntry.serialize(life_entry)), 201,
            {'Location': url_for('get_life_entry', id=life_entry.id, _external=True)})


@app.route('/api/life_entries/<int:id>')
@auth.login_required
def get_life_entry(id):
    life_entry = LifeEntry.query.get(id)
    if not life_entry:
        abort(400)
    if life_entry.user_id != g.user.id:
        abort(401)
    return jsonify(LifeEntry.serialize(life_entry))


@app.route('/api/life_entries/<int:id>', methods=['PUT'])
@auth.login_required
def update_life_entry(id):
    life_entry = LifeEntry.query.get(id)
    if not life_entry:
        abort(400)
    if life_entry.user_id != g.user.id:
        abort(401)

    request_start_time = request.json.get('start_time')
    request_end_time = request.json.get('end_time')

    start_time = datetime.strptime(request_start_time, '%H:%M:%S').time()
    if request_end_time:
        end_time = datetime.strptime(request_end_time, '%H:%M:%S').time()
    else:
        end_time = None

    life_entry.start_time = start_time
    life_entry.end_time = end_time

    db.session.commit()

    return jsonify(LifeEntry.serialize(life_entry))


@app.route('/api/life_entries/<int:id>', methods=['DELETE'])
@auth.login_required
def delete_life_entry(id):
    life_entry = LifeEntry.query.get(id)
    if not life_entry:
        abort(400)
    if life_entry.user_id != g.user.id:
        abort(401)

    db.session.query(LifeEntryActivity).filter_by(life_entry_id=life_entry.id).delete()
    db.session.delete(life_entry)

    db.session.commit()
    return ''


@app.route('/api/life_entries/search', methods=['POST'])
@auth.login_required
def search_life_entries():
    activity_id = request.json.get('activity_id')
    activity_type_id = request.json.get('activity_type_id')
    start_date = request.json.get('start_date')
    end_date = request.json.get('end_date')
    text = request.json.get('text')

    query = db.session.query(LifeEntryActivity.description, LifeEntryActivity.quantity,
                             LifeEntryActivity.rating).filter(LifeEntryActivity.user_id == g.user.id). \
        add_column(Day.id).add_column(Day.date).add_column(LifeEntry.start_time).add_column(LifeEntry.end_time). \
        add_column(Activity.name).add_column(ActivityType.name). \
        join(LifeEntry). \
        join(Day). \
        join(LifeEntryActivity.activity). \
        join(Activity.activity_type). \
        with_labels().order_by(Day.date.desc(), LifeEntry.start_time.desc())

    no_parameters = True

    if activity_id is not None:
        query = query.filter(LifeEntryActivity.activity_id == activity_id)
        no_parameters = False

    if activity_type_id is not None:
        query = query.filter(Activity.activity_type_id == activity_type_id)
        no_parameters = False

    if start_date is not None:
        query = query.filter(Day.date >= start_date)
        no_parameters = False

    if end_date is not None:
        query = query.filter(Day.date <= end_date)
        no_parameters = False

    if text is not None:
        text = '%' + text + '%'
        query = query.filter(
            or_(Activity.name.like(text), ActivityType.name.like(text), LifeEntryActivity.description.like(text)))
        no_parameters = False

    if no_parameters:
        query_result = []
    else:
        query_result = query.all()

    def serialize(result_row):
        return {
            'day_id': result_row.id,
            'date': get_date_string(result_row.date),
            'start_time': get_time_string(result_row.start_time),
            'end_time': get_time_string(result_row.end_time),
            'description': result_row.description,
            'quantity': result_row.quantity,
            'rating': result_row.rating,
            'activity_type_name': result_row.name,
            'activity_name': result_row[7]
        }

    serialized_array = [serialize(result_row) for result_row in query_result]
    return Response(json.dumps(serialized_array), mimetype='application/json')


@app.route('/api/life_entry_activities', methods=['POST'])
@auth.login_required
def new_life_entry_activity():
    user_id = g.user.id
    life_entry_id = request.json.get('life_entry_id')
    activity_id = request.json.get('activity_id')
    description = request.json.get('description')
    quantity = request.json.get('quantity')
    rating = request.json.get('rating')

    life_entry = LifeEntry.query.get(life_entry_id)
    if not life_entry:
        abort(400)
    if life_entry.user_id != g.user.id:
        abort(401)

    activity = Activity.query.get(activity_id)
    if not activity:
        abort(400)
    if activity.user_id != g.user.id:
        abort(401)

    life_entry_activity = LifeEntryActivity()
    life_entry_activity.created_date = datetime.utcnow()
    life_entry_activity.user_id = user_id
    life_entry_activity.life_entry_id = life_entry_id
    life_entry_activity.activity_id = activity_id
    life_entry_activity.description = description
    life_entry_activity.quantity = quantity
    life_entry_activity.rating = rating

    db.session.add(life_entry_activity)
    db.session.commit()
    return (jsonify(LifeEntryActivity.serialize(life_entry_activity)), 201,
            {'Location': url_for('get_life_entry_activity', id=life_entry_activity.id, _external=True)})


@app.route('/api/life_entry_activities/<int:id>')
@auth.login_required
def get_life_entry_activity(id):
    life_entry_activity = LifeEntryActivity.query.get(id)
    if not life_entry_activity:
        abort(400)
    if life_entry_activity.user_id != g.user.id:
        abort(401)
    return jsonify(LifeEntryActivity.serialize(life_entry_activity))


@app.route('/api/life_entry_activities/<int:id>', methods=['PUT'])
@auth.login_required
def update_life_entry_activity(id):
    life_entry_activity = LifeEntryActivity.query.get(id)
    if not life_entry_activity:
        abort(400)
    if life_entry_activity.user_id != g.user.id:
        abort(401)

    activity_id = request.json.get('activity_id')
    description = request.json.get('description')
    quantity = request.json.get('quantity')
    rating = request.json.get('rating')

    activity = Activity.query.get(activity_id)
    if not activity:
        abort(400)
    if activity.user_id != g.user.id:
        abort(401)

    life_entry_activity.activity_id = activity_id
    life_entry_activity.description = description
    life_entry_activity.quantity = quantity
    life_entry_activity.rating = rating

    db.session.commit()

    return jsonify(LifeEntryActivity.serialize(life_entry_activity))


@app.route('/api/life_entry_activities/<int:id>', methods=['DELETE'])
@auth.login_required
def delete_life_entry_activity(id):
    life_entry_activity = LifeEntryActivity.query.get(id)
    if not life_entry_activity:
        abort(400)
    if life_entry_activity.user_id != g.user.id:
        abort(401)

    db.session.delete(life_entry_activity)
    db.session.commit()

    return ''
