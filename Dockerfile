FROM python:3.7

COPY . /app

WORKDIR /app
RUN pip install -r requirements.txt

ENTRYPOINT gunicorn -c gunicorn.py api:app
